

% In questo demo vogliamo mostrare la differenza di impatto
% di un filtro a fase non  lineare su un segnale audio
% e su un'immagine
%
% Il nostro piano di battaglia è il seguente
%
% - Crea un filtro passa-tutto con la fase fortemente
%   non lineare
%
% - Verifica che sia un passa tutto
%
% - Applicalo ad un file audio ed ad un'immagine e
% verifica il risultato

% Per prima cosa dobbiamo creare un filtro passa-tutto.
% La ricetta la conosciamo: il denominatore deve essere il
% numeratore ribaltato.  Scegliamo il numeratore con
% gli zeri distanti dalla circonferenza di raggio unitario
% in modo che i poli risultino vicini alla circonferenza.
% Creiamo il numeratore come prodotto di celle del
% ordine due


new_cella = @(r, f) poly(r*exp([1, -1]*j*2*pi*f));

cella1 = new_cella(1.01, 1/6);
cella2 = new_cella(1.02, 1/5.8);
cella3 = new_cella(1.05, 1/5);

% Ora "moltiplichiamo" le celle.  Il prodotto tra polinomi non
% è altro che una convoluzione

num = conv(cella1, conv(cella2, cella3));

den = fliplr(num);
den = den / den(1);  % Normalizzazione

close all


h=filter(num, den, [1, zeros(1, 299)]); % Impulse response

[H, w]=freqz(num, den);                % Frequency response


figure

subplot(2,1,1);
plot(h);
xlabel('time (normalized)')
title 'impulse response'

subplot(2,1,2)
f=w/(2*pi);
handles = plotyy(f, abs(H), f, unwrap(arg(H)));
set(handles(2), 'ylabel', 'phase (rad)')
set(handles(1), 'ylabel', 'Amplitude')

title('Frequency response')
xlabel('Frequency (normalized to sampling freq.')

pause



t=-50:50;
low_pass = sinc(t*0.5);

gradino = 0.7*(t >= 0);

filtered_low_pass = filter(num, den, low_pass);
filtered_gradino = filter(num, den, gradino);

figure
subplot(2,1,1)
plot(t, low_pass, 'b', t, gradino, 'r');
title originale

subplot(2,1,2)
plot(t, filtered_low_pass, 'b', t, filtered_gradino, 'r')
title filtrata

pause


figure

% Esperimento con immagini

ori = imread('Barbara.png');
ori = double(ori);

% Filtraggio separabile

filtrata = ori;

for iter=1:2
  for row=1:size(filtrata, 1)
    filtrata(row, :) = filter(num, den, filtrata(row, :));
  end

  filtrata=filtrata'; % Ora opera sulle colonne
end

subplot(1,2,1)
imagesc(ori)
colormap gray
axis square
title('Originale')

subplot(1,2,2)
imagesc(filtrata)
title('Filtrata')
axis square
colormap gray

pause

%

[ori_audio,fs]=audioread ('islabonita.wav');

ori_audio=ori_audio(1:(fs*10), 1);

filt_audio = filter(num, den, ori_audio);

disp('originale')
soundsc(ori_audio, fs);

disp('filtrata')
soundsc(filt_audio, fs);


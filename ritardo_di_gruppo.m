   %
   %  We want to demonstrate the fact that the "group delay" is the
   %  delay that experiences a small "wavelet" centered around a given
   %  frequency.
   %
   %  We create the wavelet by modulating a low-pass window
   %  The easiest way to create said window is by convolving a rect
   %  few times
   %

L_window = 100;
   
   rect1 = ones(1, L_window);
   rect2 = (1/L_window)*conv(rect1, rect1);
   rect4 = (1/L_window)*conv(rect2, rect2);

				%
				% Now we need a all-pass filter
				%
   new_cella = @(r, f) poly(r*exp([1, -1]*j*2*pi*f));

   cella1 = new_cella(1.01, 1/6);
   cella2 = new_cella(1.02, 1/5.8);
   cella3 = new_cella(1.05, 1/5);

   % Ora "moltiplichiamo" le celle.  Il prodotto tra polinomi non
   % è altro che una convoluzione

   num = conv(cella1, conv(cella2, cella3));

   den = fliplr(num);
   den = den / den(1);  % Normalizzazione


   %
   % Now we modulate the window with two suitable cosines
   %

   f = [0.34, 0.37] / 2; % Matlab convention

   signal = zeros(2, length(g4));

   for k=1:2
     signal(k, :) = g4 .* cos(2*pi*(1:length(g4))*f(k));
   end

   %
   % Now filter with the all-pass filter
   %

   filtered=zeros(size(signal));
   
   for k=1:2
     filtered(k,:) = filter(num, den, signal(k,:));
   end

   %
   % Now, let's plot.  First check that filter is all pass
   %

   figure(1)
   clf
   freqz(num, den);

   %pause
   
   %
   % Now compare the frequency contents of the two signals with
   % the filter phase
   %

   figure(2)
   clf

   [H, w]=freqz(num, den);  % Frequency response

   Signal_Fourier = [];
   
   for k=1:2
     [tmp, w] = freqz(signal(k,:));
     Signal_Fourier = [Signal_Fourier; tmp(:)'];
   end

   ax=plotyy(w/pi, unwrap(arg(H)), ... % filter phase
	     w/pi, abs(Signal_Fourier(1,:)));

   axis(ax(2));
   hold on
   plot(ax(2), w/pi, abs(Signal_Fourier(2,:)));

   xlim(ax(1), [0.25 0.45])
   xlim(ax(2), [0.25 0.45])

   xlabel('Normalized frequency (Matlab style)')

   figure(3)

   t = (1:length(signal(1,:))) - 1;
   
   for k=1:2
     subplot(4,1,k)
     plot(t, filtered(k,:))
     title(sprintf('Filtered %d', k));
     
     subplot(4,1,k+2)
     plot(t, signal(k,:))
     title(sprintf('Original %d', k));
   end
   

# Linear phase

This repository contains few Octave/Matlab scripts that demonstrate
the importance of phase in image and audio.  You can find here

* `demo_fase_lineare.m` that filters both an image and an audio signal
  with an all-pass filter and shows/plays the result.
* `image_mix.m` This script mixes the amplitude of the Fourier
  transform of an image with the phase of another image to show that
  the most important part is actually the phase.

  The scripts work with Octave and most probably they work with Matlab too.

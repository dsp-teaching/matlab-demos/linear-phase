 %
 % This script octave demonstrates the importance of the phase in
 % images by means of a nice experiment: build an image using the
 % amplitude of the Fourier transform of an image and the phase
 % of another image and look at the result.
 %

 %
 % Load the two images
 %
barbara = double(imread('Barbara.png'));
peppers = double(imread('Peppers.png'));

 %
 % Barbara is 512x512, Peppers is 256x256.  Do a brutal subsampling
 % to make the two sizes equal
 %
barbara = barbara(1:2:end, 1:2:end);

 %
 % Check that the sizes are equal
 %
if any(size(barbara) != size(peppers))
  error('Unequal sizes')
end

 %
 % Get the Fourier transforms
 %
barbara_freq = fft2(barbara);
peppers_freq = fft2(peppers);

 %
 % Get the amplitude of the first image and the phase of the second
 % one.  The "1.0e-16" added to the Fourier transform of Peppers
 % protects us from any zero value.  Note that, to be honest,
 % Phase_peepers is not the phase, but the complex exponential
 % with said phase
 %
Amplitude_barbara = abs(barbara_freq);
Phase_Peppers = (1.0e-16 + peppers_freq) ./ abs(peppers_freq + 1.0e-16);

 %
 % Mix the two components and get the FFT inverse.  The result usually
 % have a tiny imaginary part due to rounding, remove that with
 % REAL()
 %
Mix_barbara_peppers_freq = Amplitude_barbara .* Phase_Peppers;
mix_barbara_peppers = real(ifft2(Mix_barbara_peppers_freq));

 %
 % Now create a "fake" amplitude equal to 1/sqrt(fx^2+fy^2)
 % The idea is that the amplitude of the Fourier transform
 % of a typical image decays as 1/r because of the discontinuites
 % in the image
 %

[y_size, x_size] = size(barbara);

fx = (0:(x_size-1)) ./ x_size - 1/2;
fy = (0:(y_size-1)) ./ y_size - 1/2;

[xx, yy]=meshgrid(fx, fy);

r = sqrt(xx .^ 2 + yy .^ 2);

 %
 % We use ifftshift because the origin is in the middle of r, but
 % we want it in the corners 
 %
Amplitude_fake = ifftshift(1 ./ r);

 %
 % Cut away the "infinity" around the zero that would cause
 % NaN and stuff
 %
threshold = 10000;
too_large = find(Amplitude_fake > threshold);
Amplitude_fake(too_large) = threshold;

 %
 % Now mix the fake amplitude with the right phase
 %
Mix_fake_peppers_freq = Amplitude_fake .* Phase_Peppers;
mix_fake_peppers = real(ifft2(Mix_fake_peppers_freq));

 %
 % It is plotting time!
 %



figure

subplot(2,2,1)
imagesc(barbara)
colormap gray
axis square
title Barbara

subplot(2,2,2)
imagesc(peppers)
colormap gray
axis square
title Peppers

subplot(2,2,3)
imagesc(mix_barbara_peppers)
colormap gray
axis square
title('Amplitude barbara & Phase peppers')

subplot(2,2,4)
imagesc(100*mix_fake_peppers)
colormap gray
axis square
title('Fake amplitude & Phase peppers')



